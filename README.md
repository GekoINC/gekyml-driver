# Gekyml
## Install
to install gekyml donwload `gekyml.py` and import it to your project.
## Decode 
### JSON
Here is an example of how to decode `.gekyml` files.
```python
import gekyml

with open("yourfile") as f:
    decoded=gekyml.decode(f)
```
## Syntax
`.gekyml` uses YAML syntax with some additions.
```
!importf ... imports a files contents
!importfb ... imports a files binary content
!datasheet [Datasheet name]=[YAML section to import] (Examples coming soon)
```