import pathlib
import ruamel.yaml
import sys

# Begin .gekyml specific items

class Imp(ruamel.yaml.YAMLObject):
    yaml_tag = '!importf'
    def __init__(self, val):
        self.val = val

    @classmethod
    def from_yaml(cls, loader, node):
        with open(node.value) as f:
            filed = f.read()
        return filed

class Impb(ruamel.yaml.YAMLObject):
    yaml_tag = '!importfb'
    def __init__(self, val):
        self.val = val

    @classmethod
    def from_yaml(cls, loader, node):
        with open(node.value, "rb") as f:
            filed = f.read()
        return filed

class Datasheet(ruamel.yaml.YAMLObject):
    yaml_tag = '!datasheet'

    def __init__(self, key, value):
        self.key = key
        self.value = value

    @classmethod
    def from_yaml(cls, constructor, node):
        x = node.value.split("=")
        return {"name": x[0],"value": x[1], "type": "Datasheet"}
        #print(node.value)

"""class ref(ruamel.yaml.YAMLObject):
    yaml_tag = '!refm'

    def __init__(self, val):
        self.val = val

    @classmethod
    def from_yaml(cls, constructor, node):
        return {"reference": node.value, "type": "reference"}"""

# end /gekyml specific items




def decode(file_obj):
    data = ruamel.yaml.load(file_obj.read(), Loader=ruamel.yaml.Loader)

    try:
        x = data["datasheet"]
        x["value"] = data[x["value"]]
        data["datasheet"] = x
    except Exception as e:
        pass

    return data

with open("test.gekyml") as f:
    print(decode(f))